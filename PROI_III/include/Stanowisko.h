//
// Created by Michal on 2017-12-29.
//

#ifndef PROI_III_STANOWISKO_H
#define PROI_III_STANOWISKO_H

/* Klasa przodek dla kazdego ze stanowisk, w ktorej definiuje dwie metody dotyczace spawn i zdefiniowalem
 inne ktore nadpisalem w poszczegolnych potomkach, swoja droga czy daloby sie wiecej klas zdefiniowac w tej klasie?
 Jezeli kazda z metod w klasach potomkach produkuje inne obiekty (nie dosc ze stanowisko to jeszcze produkt stanowiska)
*/
class Stanowisko {


private:
    int spawn=1; // dlugosc oczekiwania na dany produkt w sekundach
public:
    Stanowisko();
    virtual ~Stanowisko();
    int getSpawn(); // funkcja zwracajaca dugosc oczekiwania
    void setSpawn(int sp); // funkcja ustalajaca dlugosc oczekiwania
    virtual int getWyprodukowane()=0; // funkcja ktora bedzie sprawdzac ilosc wyprodukowanych produktow
    bool sprawdzZapas(); // funkcja ktora bedzie sprawdzac zapas produktow w kolejkach i zwracac TRUE jesli bedzie mozna produkowac
    void skladuj(); //funkcja ktora bedzie skladowala produkty wyprodukowane w stanowiskach docelowych
};


#endif //PROI_III_STANOWISKO_H
