//
// Created by Michal on 2017-12-30.
//

#ifndef PROI_III_STANOWISKOBLAT_H
#define PROI_III_STANOWISKOBLAT_H

#include "Stanowisko.h"
#include "StanowiskoStol.h"

class StanowiskoBlat : public Stanowisko
{
private:
    int wyprodukowane=0;
public:
    StanowiskoBlat();
    ~StanowiskoBlat() override;
    int getWyprodukowane() override;
    //funkcja ktora sprawdza czy mozna produkowac (czy kolejki nie sa za duze) - analogicznie zdefiniowana w innych klasach
    bool czyProdukowac(StanowiskoStol sstol1, StanowiskoStol sstol2);
    //funkcja ktora produkuje do danych stanowisk Blat (w zaleznosci w ktorym jest mniejsza kolejka) - analogicznie zdefinowana w innych klasach
    void produkuj(StanowiskoStol* sstol1, StanowiskoStol* sstol2);
};



#endif //PROI_III_STANOWISKOBLAT_H
