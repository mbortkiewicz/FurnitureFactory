//
// Created by Michal on 2018-01-03.
//

#ifndef PROI_III_STANOWISKOMONTAZDIZ_H
#define PROI_III_STANOWISKOMONTAZDIZ_H


#include "Stanowisko.h"
#include "Drzwiczki.h"
#include "Zawiasy.h"
#include "MontazDiZ.h"
#include <queue>
#include <iostream>
#include "StanowiskoSzaf.h"

class StanowiskoMontazDiZ : public Stanowisko {
    // poniewaz potrzebny bedzie dostep do zwiekszania kolejki w danych stanowiskach (w innych klasach analogicznie)
    friend class StanowiskoDrzwiczek;
    friend class StanowiskoZawiasy;
private:

    int wyprodukowane=0;
    std::queue <MontazDiZ> wyprodukowaneMontaze; // kolejka wyprodukowanych produktow (w innych klasach analogicznie)
    std::queue <Zawiasy> zapasZawiasow; // kolejka skladowego produktu (w innych klasach analogicznie)
    std::queue <Drzwiczki> zapasDrzwiczek;
public:
    StanowiskoMontazDiZ();
    ~StanowiskoMontazDiZ() override;
    int getWyprodukowane() override;
    int getIloscDrzwiczek(); // zwraca dlugosc kolejki drzwiczek - analogiczne metody gdzie indziej
    int getIloscZawiasow();
    bool sprawdzZapas();
    void skladuj();
    bool czyProdukowac(StanowiskoSzaf sstol1, StanowiskoSzaf sstol2);
    void produkuj(StanowiskoSzaf* sstol1, StanowiskoSzaf* sstol2);

};



#endif //PROI_III_STANOWISKOMONTAZDIZ_H
