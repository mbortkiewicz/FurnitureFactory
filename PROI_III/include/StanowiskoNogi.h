//
// Created by Michal on 2017-12-30.
//

#ifndef PROI_III_STANOWISKONOG_H
#define PROI_III_STANOWISKONOG_H


#include "Stanowisko.h"
#include "StanowiskoStol.h"

class StanowiskoNogi : public Stanowisko
{

private:

    int wyprodukowane=0;
public:
    StanowiskoNogi();
    ~StanowiskoNogi() override; //overriduje destruktor klasy abstrakcyjnej stanowisko
    int getWyprodukowane() override;
    bool czyProdukowac(StanowiskoStol sstol1, StanowiskoStol sstol2);
    void produkuj(StanowiskoStol* sstol1, StanowiskoStol* sstol2); //produkuje do danego stanowiska

};


#endif //PROI_III_STANOWISKONOG_H
