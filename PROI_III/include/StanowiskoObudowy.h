//
// Created by Michal on 2018-01-03.
//

#ifndef PROI_III_STANOWISKOOBUDOWY_H
#define PROI_III_STANOWISKOOBUDOWY_H


#include "Stanowisko.h"
#include "StanowiskoSzaf.h"

class StanowiskoObudowy : public Stanowisko{
private:

    int wyprodukowane=0;
public:
    StanowiskoObudowy();
    ~StanowiskoObudowy() override;
    int getWyprodukowane() override;
    bool czyProdukowac(StanowiskoSzaf sstol1, StanowiskoSzaf sstol2);
    void produkuj(StanowiskoSzaf* sstol1, StanowiskoSzaf* sstol2);



};


#endif //PROI_III_STANOWISKOOBUDOWY_H
