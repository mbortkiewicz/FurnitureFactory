//
// Created by Michal on 2017-12-30.
//

#ifndef PROI_III_STANOWISKOSTOL_H
#define PROI_III_STANOWISKOSTOL_H


#include "Stanowisko.h"
#include <iostream>
#include <queue>
#include "Stol.h"
#include "Nogi.h"
#include "Blat.h"

class StanowiskoStol : public Stanowisko
{
    friend class StanowiskoNogi;
    friend class StanowiskoBlat;
private:

    int wyprodukowane=0;
    std::queue <Stol> wyprodukowaneStoly;
    std::queue <Nogi> zapasNog;
    std::queue <Blat> zapasBlatow;

public:
    StanowiskoStol();
    ~StanowiskoStol();
    int getWyprodukowane() override;
    int getIloscNog();
    int getIloscBlatow();
    bool sprawdzZapas();
    void skladuj();
};


#endif //PROI_III_STANOWISKOSTOL_H
