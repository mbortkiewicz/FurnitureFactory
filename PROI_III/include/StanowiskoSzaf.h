//
// Created by Michal on 2018-01-03.
//

#ifndef PROI_III_STANOWISKOSZAF_H
#define PROI_III_STANOWISKOSZAF_H

#include "MontazDiZ.h"
#include "Obudowy.h"
#include "Szafa.h"
#include "Stanowisko.h"
#include <queue>
#include <iostream>



class StanowiskoSzaf :public Stanowisko
{

    friend class StanowiskoObudowy;
    friend class StanowiskoMontazDiZ;
private:

    int wyprodukowane=0;
    std::queue <Szafa> wyprodukowaneSzafy;
    std::queue <Obudowy> zapasObudow;
    std::queue <MontazDiZ> zapasMontazy;

public:
    StanowiskoSzaf();
    ~StanowiskoSzaf() override;
    int getWyprodukowane() override;
    int getIloscObudow();
    int getIloscMontazy();
    bool sprawdzZapas();
    void skladuj();

};


#endif //PROI_III_STANOWISKOSZAF_H
