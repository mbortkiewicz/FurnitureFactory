//
// Created by Michal on 2018-01-03.
//

#ifndef PROI_III_STANOWISKOZAWIASY_H
#define PROI_III_STANOWISKOZAWIASY_H


#include "Stanowisko.h"
#include "StanowiskoMontazDiZ.h"

class StanowiskoZawiasy : public Stanowisko{
private:

    int wyprodukowane=0;
public:
    StanowiskoZawiasy();
    ~StanowiskoZawiasy() override;

    int getWyprodukowane() override;
    bool czyProdukowac(StanowiskoMontazDiZ sstol1, StanowiskoMontazDiZ sstol2);
    void produkuj(StanowiskoMontazDiZ* sstol1, StanowiskoMontazDiZ* sstol2);

};




#endif //PROI_III_STANOWISKOZAWIASY_H
