#include <iostream>
#include <fstream>
#include <queue>
#include <time.h>
#include "Stanowisko.h"
#include "StanowiskoBlat.h"
#include "StanowiskoNogi.h"
#include "StanowiskoStol.h"
#include "StanowiskoMontazDiZ.h"
#include "StanowiskoDrzwiczek.h"
#include "StanowiskoZawiasy.h"
#include "StanowiskoSzaf.h"
#include "StanowiskoObudowy.h"
#include "Nogi.h"
#include "Stol.h"
#include "Blat.h"

using namespace std;


void czekaj() // funkcja wymuszajaca czekania sekundy po kazdej iteracji glownego fora
{
    clock_t koniec_czekania;
    koniec_czekania = clock() + 1 * CLOCKS_PER_SEC;
    while( clock() < koniec_czekania ) { }
}

int main() {

    // Tworzenie pliku do zapisu symulacji
    ofstream myfile;
    myfile.open ("Symulacja.txt");
    myfile << "Fabryka stolow i szaf:\n";


    // Tworzenie obiektow poszczegolnych stanowisk
    StanowiskoStol sStol1;
    StanowiskoStol sStol2;
    StanowiskoBlat sBlat1;
    StanowiskoBlat sBlat2;
    StanowiskoNogi sNogi1;
    StanowiskoNogi sNogi2;
    StanowiskoNogi sNogi3;
    StanowiskoNogi sNogi4;
    StanowiskoDrzwiczek sDrzwiczki1;
    StanowiskoDrzwiczek sDrzwiczki2;
    StanowiskoZawiasy sZawiasy1;
    StanowiskoZawiasy sZawiasy2;
    StanowiskoMontazDiZ sMDiZ1;
    StanowiskoMontazDiZ sMDiZ2;
    StanowiskoObudowy sO1;
    StanowiskoObudowy sO2;
    StanowiskoSzaf sSz1;
    StanowiskoSzaf sSz2;

    //ustawianie czasu produkcji przedmiotu na danym stanowisku
    sStol1.setSpawn(2);
    sStol2.setSpawn(2);
    sBlat1.setSpawn(1);
    sBlat2.setSpawn(1);
    sNogi1.setSpawn(1);
    sNogi2.setSpawn(1);
    sNogi3.setSpawn(1);
    sNogi4.setSpawn(1);
    sDrzwiczki1.setSpawn(1);
    sDrzwiczki2.setSpawn(1);
    sZawiasy1.setSpawn(2);
    sZawiasy2.setSpawn(2);
    sMDiZ1.setSpawn(3);
    sMDiZ2.setSpawn(3);
    sO1.setSpawn(2);
    sO2.setSpawn(2);
    sSz1.setSpawn(4);
    sSz2.setSpawn(4);

    /*glowna petla odpowaiadajaca za dzialanie fabryki - kazde stanowisko sprawdza w
    pierwszym ifie czy "juz czas cos wyprodukowac" nastepnie stanowiska zrodlowe (czyli
    te ktore nie posiadaja kolejek produktow) i posrednie (sMDiZ) sprawdzaja czy mozna podukowac - to znaczy czy
    kolejki w stanowiskach do ktorych produkuja nie sa za dlugie, natomiast stanowiska docelowe i zarazem posrednie (sMDiZ)
    sprawdzaja czy moga wyprodukowac przedmiot (sprawdzaja ilosc zapasow w kolejkach)
    */
    for(int i=0;i<=14;i++)
    {
        { // Stanowiska Nog
            if (i % sNogi1.getSpawn() == 0) {
                if (sNogi1.czyProdukowac(sStol1, sStol2) == 1) {
                    sNogi1.produkuj(&sStol1, &sStol2);
                }
            }

            if (i % sNogi2.getSpawn() == 0) {
                if (sNogi2.czyProdukowac(sStol1, sStol2) == 1) {
                    sNogi2.produkuj(&sStol1, &sStol2);
                }
            }

            if (i % sNogi3.getSpawn() == 0) {
                if (sNogi3.czyProdukowac(sStol1, sStol2) == 1) {
                    sNogi3.produkuj(&sStol1, &sStol2);
                }
            }

            if (i % sNogi4.getSpawn() == 0) {
                if (sNogi4.czyProdukowac(sStol1, sStol2) == 1) {
                    sNogi4.produkuj(&sStol1, &sStol2);
                }
            }
        }

        { // Stanowiska Blatow
            if (i % sBlat1.getSpawn() == 0) {
                if (sBlat1.czyProdukowac(sStol1, sStol2) == 1) {
                    sBlat1.produkuj(&sStol1, &sStol2);
                }
            }

            if (i % sBlat2.getSpawn() == 0) {
                if (sBlat2.czyProdukowac(sStol1, sStol2) == 1) {
                    sBlat2.produkuj(&sStol1, &sStol2);
                }
            }
        }


        { // Stanowiska Stolow
            if(i % sStol1.getSpawn() == 0)
                if(sStol1.sprawdzZapas())
                    sStol1.skladuj();

            if(i % sStol2.getSpawn() == 0)
                if(sStol2.sprawdzZapas())
                    sStol2.skladuj();
        }

        { // Stanowiska Drzwiczek
            if(i % sDrzwiczki1.getSpawn() == 0)
            {
                if (sDrzwiczki1.czyProdukowac(sMDiZ1, sMDiZ2) == 1) {
                    sDrzwiczki1.produkuj(&sMDiZ1, &sMDiZ2);
                }
            }
            if(i % sDrzwiczki2.getSpawn() == 0)
            {
                if (sDrzwiczki2.czyProdukowac(sMDiZ1, sMDiZ2) == 1) {
                    sDrzwiczki2.produkuj(&sMDiZ1, &sMDiZ2);
                }
            }
        }

        { // Stanowisko Zawiasow
            if(i % sZawiasy1.getSpawn() == 0)
            {
                if (sZawiasy1.czyProdukowac(sMDiZ1, sMDiZ2) == 1) {
                    sZawiasy1.produkuj(&sMDiZ1, &sMDiZ2);
                }
            }

            if(i % sZawiasy2.getSpawn() == 0)
            {
                if (sZawiasy2.czyProdukowac(sMDiZ1, sMDiZ2) == 1) {
                    sZawiasy2.produkuj(&sMDiZ1, &sMDiZ2);
                }
            }

        }

        { // Stanowiska MontazDiZ
            if(i % sMDiZ1.getSpawn() == 0)
                if(sMDiZ1.sprawdzZapas())
                    if(sMDiZ1.czyProdukowac(sSz1,sSz2))
                        sMDiZ1.produkuj(&sSz1,&sSz2);

            if(i % sMDiZ2.getSpawn() == 0)
                if(sMDiZ2.sprawdzZapas())
                    if(sMDiZ2.czyProdukowac(sSz1,sSz2))
                        sMDiZ2.produkuj(&sSz1,&sSz2);
        }

        { // Stanowisko Obudow
            if(i % sO1.getSpawn() == 0)
            {
                if (sO1.czyProdukowac(sSz1, sSz2) == 1) {
                    sO1.produkuj(&sSz1, &sSz2);
                }
            }
            if(i % sO2.getSpawn() == 0)
            {
                if (sO2.czyProdukowac(sSz1, sSz2) == 1) {
                    sO2.produkuj(&sSz1, &sSz2);
                }
            }
        }

        { // Stanowisko Szaf
            if(i % sSz1.getSpawn() == 0)
                if(sSz1.sprawdzZapas())
                    sSz1.skladuj();

            if(i % sSz2.getSpawn() == 0)
                if(sSz2.sprawdzZapas())
                    sSz2.skladuj();
        }


        // wyswietlanie kazdego ze stanowisk (posrednich i docelowych) kolejek oraz ilosci wyprodukowanych przedmiotow
        std::cout << "MINELO " << i <<" j. czasu" << std::endl;
        myfile<< "MINELO " << i <<" j. czasu" << std::endl;


        std::cout <<  "STANOWISKO Stol1:" << std:: endl; //czy mozna drukowac i zapisywac za jednym zamachem?
        myfile <<  "STANOWISKO Stol1:" << std:: endl;

        std::cout << "kolejka blatow: " << sStol1.getIloscBlatow() << "\t||\tkolejka nog: " << sStol1.getIloscNog() <<  "\t\t||\twyprodukowane stoly: " << sStol1.getWyprodukowane() << "\n";
        myfile << "kolejka blatow: " << sStol1.getIloscBlatow() << "\t||\tkolejka nog: " << sStol1.getIloscNog() <<  "\t\t||\twyprodukowane stoly: " << sStol1.getWyprodukowane() << "\n";

        std::cout << "STANOWISKO Stol2:" << std:: endl;
        myfile << "STANOWISKO Stol2:" << std:: endl;

        std::cout << "kolejka blatow: " << sStol2.getIloscBlatow() << "\t||\tkolejka nog: " << sStol2.getIloscNog() <<  "\t\t||\twyprodukowane stoly: " << sStol2.getWyprodukowane() << "\n";
        myfile << "kolejka blatow: " << sStol2.getIloscBlatow() << "\t||\tkolejka nog: " << sStol2.getIloscNog() <<  "\t\t||\twyprodukowane stoly: " << sStol2.getWyprodukowane() << "\n";

        std::cout << "STANOWISKO MontazuDiZ1:" << std:: endl;
        myfile << "STANOWISKO MontazuDiZ1:" << std:: endl;

        std::cout << "kolejka drzwiczek: " << sMDiZ1.getIloscDrzwiczek() << "\t||\tkolejka zawiasow: " << sMDiZ1.getIloscZawiasow() <<  "\t||\twyprodukowane montaze: " << sMDiZ1.getWyprodukowane() << "\n";
        myfile << "kolejka drzwiczek: " << sMDiZ1.getIloscDrzwiczek() << "\t||\tkolejka zawiasow: " << sMDiZ1.getIloscZawiasow() <<  "\t||\twyprodukowane montaze: " << sMDiZ1.getWyprodukowane() << "\n";

        std::cout << "STANOWISKO MontazuDiZ2:" << std:: endl;
        myfile << "STANOWISKO MontazuDiZ2:" << std:: endl;

        std::cout << "kolejka drzwiczek: " << sMDiZ2.getIloscDrzwiczek() << "\t||\tkolejka zawiasow: " << sMDiZ2.getIloscZawiasow() <<  "\t||\twyprodukowane montaze: " << sMDiZ2.getWyprodukowane() << "\n";
        myfile << "kolejka drzwiczek: " << sMDiZ2.getIloscDrzwiczek() << "\t||\tkolejka zawiasow: " << sMDiZ2.getIloscZawiasow() <<  "\t||\twyprodukowane montaze: " << sMDiZ2.getWyprodukowane() << "\n";

        std::cout << "STANOWISKO Szaf1:" << std:: endl;
        myfile << "STANOWISKO Szaf1:" << std:: endl;

        std::cout << "kolejka montazy: " << sSz1.getIloscMontazy() << "\t||\tkolejka obudowy: " << sSz1.getIloscObudow() <<  "\t||\twyprodukowane montaze: " << sSz1.getWyprodukowane() << "\n";
        myfile << "kolejka montazy: " << sSz1.getIloscMontazy() << "\t||\tkolejka obudowy: " << sSz1.getIloscObudow() <<  "\t||\twyprodukowane montaze: " << sSz1.getWyprodukowane() << "\n";

        std::cout << "STANOWISKO Szaf2:" << std:: endl;
        myfile << "STANOWISKO Szaf2:" << std:: endl;

        std::cout << "kolejka montazy: " << sSz2.getIloscMontazy() << "\t||\tkolejka obudowy: " << sSz2.getIloscObudow() <<  "\t||\twyprodukowane montaze: " << sSz2.getWyprodukowane() << "\n";
        myfile << "kolejka montazy: " << sSz2.getIloscMontazy() << "\t||\tkolejka obudowy: " << sSz2.getIloscObudow() <<  "\t||\twyprodukowane montaze: " << sSz2.getWyprodukowane() << "\n";

        std::cout << "\n----------------------------------------------------------------------------------\n" << std:: endl;
        myfile << "\n----------------------------------------------------------------------------------\n" << std:: endl;


        czekaj();

    }

    myfile.close();

    return 0;
}
