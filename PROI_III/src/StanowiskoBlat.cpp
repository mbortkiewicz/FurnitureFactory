//
// Created by Michal on 2017-12-30.
//

#include "StanowiskoBlat.h"

StanowiskoBlat::StanowiskoBlat()
{

}

StanowiskoBlat::~StanowiskoBlat()
{

}



int StanowiskoBlat::getWyprodukowane()
{
    return wyprodukowane;
}

bool StanowiskoBlat::czyProdukowac(StanowiskoStol sstol1, StanowiskoStol sstol2) {
    if(sstol1.getIloscBlatow()>=10 && sstol2.getIloscBlatow()>=10)
        return false;
    else
        return true;
}

void StanowiskoBlat::produkuj(StanowiskoStol* sstol1, StanowiskoStol* sstol2)
{
    if(sstol1->getIloscBlatow() <= sstol2->getIloscBlatow())
        sstol1->zapasBlatow.push(Blat());
    else
        sstol2->zapasBlatow.push(Blat());
    wyprodukowane+=1;
}
