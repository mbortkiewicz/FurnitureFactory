//
// Created by Michal on 2018-01-03.
//

#include "StanowiskoDrzwiczek.h"

StanowiskoDrzwiczek::StanowiskoDrzwiczek() {

}

StanowiskoDrzwiczek::~StanowiskoDrzwiczek() {

}



int StanowiskoDrzwiczek::getWyprodukowane() {
    return wyprodukowane;
}

bool StanowiskoDrzwiczek::czyProdukowac(StanowiskoMontazDiZ sstol1, StanowiskoMontazDiZ sstol2) {
    if(sstol1.getIloscDrzwiczek()>=10 && sstol2.getIloscDrzwiczek()>=10)
        return false;
    else
        return true;
}

void StanowiskoDrzwiczek::produkuj(StanowiskoMontazDiZ *sstol1, StanowiskoMontazDiZ *sstol2) {
    if(sstol1->getIloscDrzwiczek() <= sstol2->getIloscDrzwiczek())
        sstol1->zapasDrzwiczek.push(Drzwiczki());
    else
        sstol2->zapasDrzwiczek.push(Drzwiczki());
    wyprodukowane+=1;
}
