//
// Created by Michal on 2018-01-03.
//

#include "StanowiskoMontazDiZ.h"

StanowiskoMontazDiZ::StanowiskoMontazDiZ() {

}

StanowiskoMontazDiZ::~StanowiskoMontazDiZ() {

}


int StanowiskoMontazDiZ::getWyprodukowane() {
    return wyprodukowane;
}

int StanowiskoMontazDiZ::getIloscDrzwiczek() {
    return this->zapasDrzwiczek.size();
}

int StanowiskoMontazDiZ::getIloscZawiasow() {
    return this->zapasZawiasow.size();
}

bool StanowiskoMontazDiZ::sprawdzZapas() {
    if(this->zapasDrzwiczek.size()>=1&&this->zapasZawiasow.size()>=1)
        return true;
    else
        return false;
}

void StanowiskoMontazDiZ::skladuj() {
    this->wyprodukowane+=1;
    this->wyprodukowaneMontaze.push(MontazDiZ());
    this->zapasZawiasow.pop();
    this->zapasDrzwiczek.pop();
}

bool StanowiskoMontazDiZ::czyProdukowac(StanowiskoSzaf sstol1, StanowiskoSzaf sstol2) {
    if(sstol1.getIloscMontazy()>=10 &&sstol2.getIloscMontazy()>=10)
        return false;
    else
        return true;
}

void StanowiskoMontazDiZ::produkuj(StanowiskoSzaf *sstol1, StanowiskoSzaf *sstol2) {
    if(sstol1->getIloscMontazy() <= sstol2->getIloscMontazy())
        sstol1->zapasMontazy.push(MontazDiZ());
    else
        sstol2->zapasMontazy.push(MontazDiZ());
    wyprodukowane+=1;
}
