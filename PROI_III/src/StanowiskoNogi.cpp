//
// Created by Michal on 2017-12-30.
//

#include "StanowiskoNogi.h"




StanowiskoNogi::StanowiskoNogi()
{

}

StanowiskoNogi::~StanowiskoNogi()
{

}


int StanowiskoNogi::getWyprodukowane() {
    return wyprodukowane;
}

bool StanowiskoNogi::czyProdukowac(StanowiskoStol sstol1, StanowiskoStol sstol2)
{
    if(sstol1.getIloscNog()>=10 &&sstol2.getIloscNog()>=10)
        return false;
    else
        return true;
}

void StanowiskoNogi::produkuj(StanowiskoStol* sstol1, StanowiskoStol* sstol2)
{
    if(sstol1->getIloscNog() <= sstol2->getIloscNog())
        sstol1->zapasNog.push(Nogi());
    else
        sstol2->zapasNog.push(Nogi());
    wyprodukowane+=1;
}

