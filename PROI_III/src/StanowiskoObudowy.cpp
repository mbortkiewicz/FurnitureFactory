//
// Created by Michal on 2018-01-03.
//

#include "StanowiskoObudowy.h"

StanowiskoObudowy::StanowiskoObudowy() {

}

StanowiskoObudowy::~StanowiskoObudowy() {

}



int StanowiskoObudowy::getWyprodukowane() {
    return wyprodukowane;
}

bool StanowiskoObudowy::czyProdukowac(StanowiskoSzaf sstol1, StanowiskoSzaf sstol2) {
    if(sstol1.getIloscObudow()>=10 &&sstol2.getIloscObudow()>=10)
        return false;
    else
        return true;
}

void StanowiskoObudowy::produkuj(StanowiskoSzaf *sstol1, StanowiskoSzaf *sstol2) {
    if(sstol1->getIloscObudow() <= sstol2->getIloscObudow())
        sstol1->zapasObudow.push(Obudowy());
    else
        sstol2->zapasObudow.push(Obudowy());
    wyprodukowane+=1;
}
