//
// Created by Michal on 2017-12-30.
//

#include "StanowiskoStol.h"

StanowiskoStol::StanowiskoStol() {}
StanowiskoStol::~StanowiskoStol() {}



int StanowiskoStol::getWyprodukowane()
{
    return wyprodukowane;
}

void StanowiskoStol::skladuj()
{
    this->wyprodukowane+=1;
    this->wyprodukowaneStoly.push(Stol());
    this->zapasNog.pop();
    this->zapasBlatow.pop();
}

bool StanowiskoStol::sprawdzZapas()
{
    if(this->zapasBlatow.size()>=1&&this->zapasNog.size()>=1)
        return true;
    else
        return false;
}

int StanowiskoStol::getIloscNog()
{
    return this->zapasNog.size();
}

int StanowiskoStol::getIloscBlatow()
{
    return this->zapasBlatow.size();
}
