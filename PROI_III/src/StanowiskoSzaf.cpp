//
// Created by Michal on 2018-01-03.
//

#include "StanowiskoSzaf.h"

StanowiskoSzaf::StanowiskoSzaf() {}

StanowiskoSzaf::~StanowiskoSzaf() {}


int StanowiskoSzaf::getWyprodukowane() {
    return wyprodukowane;
}

int StanowiskoSzaf::getIloscObudow() {
    return this->zapasObudow.size();
}

int StanowiskoSzaf::getIloscMontazy() {
    return this->zapasMontazy.size();
}

bool StanowiskoSzaf::sprawdzZapas() {
    if(this->zapasMontazy.size()>=1&&this->zapasObudow.size()>=1)
        return true;
    else
        return false;
}

void StanowiskoSzaf::skladuj() {
    this->wyprodukowane+=1;
    this->wyprodukowaneSzafy.push(Szafa());
    this->zapasObudow.pop();
    this->zapasMontazy.pop();
}

