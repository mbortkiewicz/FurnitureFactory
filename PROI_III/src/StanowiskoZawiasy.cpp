//
// Created by Michal on 2018-01-03.
//

#include "StanowiskoZawiasy.h"

StanowiskoZawiasy::StanowiskoZawiasy() {

}

StanowiskoZawiasy::~StanowiskoZawiasy() {

}



int StanowiskoZawiasy::getWyprodukowane() {
    return wyprodukowane;
}

bool StanowiskoZawiasy::czyProdukowac(StanowiskoMontazDiZ sstol1, StanowiskoMontazDiZ sstol2) {
    if(sstol1.getIloscZawiasow()>=10 && sstol2.getIloscZawiasow()>=10)
        return false;
    else
        return true;
}

void StanowiskoZawiasy::produkuj(StanowiskoMontazDiZ *sstol1, StanowiskoMontazDiZ *sstol2) {
    if(sstol1->getIloscZawiasow() <= sstol2->getIloscZawiasow())
        sstol1->zapasZawiasow.push(Zawiasy());
    else
        sstol2->zapasZawiasow.push(Zawiasy());
    wyprodukowane+=1;
}

